"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "RoyButton", {
  enumerable: true,
  get: function get() {
    return _RoyButton.default;
  }
});

var _RoyButton = _interopRequireDefault(require("./RoyButton"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }